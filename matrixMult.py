import numpy as np

class vector:
	def __init__(self,tamanho,init=[]):
		self.tamanho = tamanho
		self.data=[0 for i in range(tamanho)]
		if len(init):
			self.data=[i for i in init]
	
	def __str__(self):
		for i in range(self.tamanho):
			string+=","+str(self.data[i][j])
			string= string.lstrip(",")
		return string 

	def dotProd(self, vectorB):
		assert(self.tamanho==vectorB.tamanho), "vetores devem ter o mesmo tamanho"
		result=0
		for i in range(self.tamanho):
			result+=self.data[i]*vectorB.data[i]
		return result

	def convertToNP(self):
		return np.array(self.data)


class matrix:
	def __init__(self, linhas, colunas):
		self.linhas=linhas
		self.colunas=colunas
		self.data=[[0 for j in range(colunas)] for i in range(linhas)] 
		
	def __str__(self):
		stringRet = ""
		for i in range(self.linhas):
			string = ""
			for j in range(self.colunas):
				string+=","+str(self.data[i][j])
			string= string.lstrip(",")
			string+="\n"
			stringRet+=string
		return stringRet


	def multiply(self, matrixB):
		assert(self.colunas==matrixB.linhas), "colunas de A tem q ser igual linhas de B"
		resultMatrix = matrix(self.linhas,matrixB.colunas)
		for linhaA in range(self.linhas):
			for colunaB in range(matrixB.colunas):
				for i in range(self.colunas):
					vectorBuf = vector(matrixB.colunas)
					for k in range(matrixB.colunas):
						vectorBuf.data[k] = matrixB.data[k][colunaB]
					resultMatrix.data[linhaA][colunaB] += vector(len(self.data[linhaA]),self.data[linhaA]).dotProd(vectorBuf)

		return resultMatrix


	def multiplyWDotP(self, matrixB):
		assert(self.colunas==matrixB.linhas), "colunas de A tem q ser igual linhas de B"
		resultMatrix = matrix(self.linhas,matrixB.colunas)
		for linhaA in range(self.linhas):
			for colunaB in range(matrixB.colunas):
				resultMatrix.data[linhaA][colunaB] += self.data[linhaA][i]*matrixB.data[i][colunaB]
	
	def convertToNP(self):
		return np.array(self.data)





mA = matrix(3,3)
mB = matrix(3,3)


mA.data=[[1,2,3],
	 [1,2,3],
	 [1,2,3]]


mB.data=[[0,2,3],
	 [0,2,3],
	 [0,2,3]]


mC = mA.multiply(mB)
mD = mA.multiply(mB)

print(mC)
print(mD)






