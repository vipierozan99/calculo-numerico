import Tkinter
from math import log10
import matplotlib
#matplotlib.use("agg")
import matplotlib.pyplot as plt
import numpy as np

def divDif(funct, array):
    if(len(array)==1):
        return funct(array[0])
    rmFirst = slice(1,None)
    rmLast = slice(0,-1)
    nume = divDif(funct, array[rmFirst])-divDif(funct,array[rmLast])
    deno = array[-1]-array[0]
    #print(array[rmFirst], array[rmLast], nume,deno)
    return (nume/deno)
    
def newtonInterp(f, array):
    coefs = []
    coefs.append(divDif(f,array))
    for i in range(len(array)-1):
        coefs.append(divDif(f,array[slice(None,-(i+1))]))
    def interpolation(x):
        revArray = array[:-1]
        #revArray = revArray[slice(1,None)]
        result = 0
        print(coefs,revArray)
        for i in range(len(coefs)):
            pointDif = 1
            for j in range(len(revArray)-i):
                pointDif=pointDif*(x-revArray[j])
                print(pointDif)
            result += coefs[i]*pointDif
        return result

    return interpolation

    



array = [1,2,3,4,5]

interpol = newtonInterp(np.log,array)
xAxis = np.linspace(0,10,1000)
plt.plot(xAxis, np.log(xAxis))
plt.plot(xAxis, interpol(xAxis))
plt.show()
print(interpol(2), np.log(2))

