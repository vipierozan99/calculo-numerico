# %%
import numericalCalc as nc


print(nc.__doc__)


def f(x):
    return x**3 + 4*x**2 - 10


def f2(x):
    return x - x**3 - 4*x**2 + 10


def f3(x):
    return 0.5*(10-x**3)**0.5


zero1 = nc.bisseccao(f, 1, 2, 0.001, graph=True)
zero2 = nc.pontoFixo(f2, 1.5, 0.001)
zero3 = nc.pontoFixo(f3, 1.5, 0.001, graph=True)

print("zero1", zero1)
print("zero2", zero2)
print("zero3", zero3)
