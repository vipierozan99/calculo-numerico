import matplotlib.pyplot as plt
import numpy as np

from dill.source import getsource


def spline1(array):
    spline = []
    subIntervals = [[a, b] for a, b in zip(array[0:-1], array[1:None])]
    xAxis = []
    i = 0
    for subI in subIntervals:
        a = subI[0][1]
        b = (subI[1][1] - subI[0][1])/(subI[1][0] - subI[0][0])
        addToSpline = [a+b*(xi-subI[0][0])
                       for xi in np.linspace(subI[0][0], subI[1][0], 100)]

        if i != len(subIntervals)-1:
            addToSpline.pop()
        spline += addToSpline
        xAxis += list(np.linspace(subI[0][0], subI[1][0], len(addToSpline)))
        i += 1

    return (xAxis, spline)


def spline2(array):
    spline = []
    subIntervals = [[a, b] for a, b in zip(array[0:-1], array[1:None])]
    coefsB = []
    coefsC = []
    coefsH = []
    xAxis = []
    for i in range(len(subIntervals)):
        subI = subIntervals[i]
        a = subI[0][1]
        divDif = (subI[1][1] - subI[0][1])/(subI[1][0] - subI[0][0])

        h = (subI[1][0] - subI[0][0])
        coefsH.append(h)

        if i == 0:
            c = 0
            b = divDif
        else:
            b = coefsB[i-1] + 2*coefsC[i-1]*coefsH[i-1]
            c = (divDif - b)/h

        coefsB.append(b)
        coefsC.append(c)
        addToSpline = [a+b*(xi - subI[0][0])+c*(xi - subI[0][0])**2
                       for xi in np.linspace(subI[0][0], subI[1][0], 100)]

        if i != len(subIntervals)-1:
            addToSpline.pop()
        spline += addToSpline
        xAxis += list(np.linspace(subI[0][0], subI[1][0], len(addToSpline)))

    return (xAxis, spline)


array = [(1.5, 13), (2.5, 5.7), (3, 12), (4, 16), (5, 10)]

xAxis, yAxis = spline1(array)
plt.plot(xAxis, yAxis)

xAxis, yAxis = spline2(array)
plt.plot(xAxis, yAxis)

for i in array:
    print(str(i))
    plt.plot(i[0], i[1], 'ro', label=str(i))
plt.show()
