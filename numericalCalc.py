"""Modulo de calculo numerico
by: vipierozan99

funcoes: bisseccao(), pontoFixo().

use: print(modulo.funcao.__doc__)  para mais informacoes
"""


from math import log10
import matplotlib.pyplot as plt
import numpy as np


def bisseccao(f, a, b, tol, graph=False, maxIt=10000):
    """bissecao(funcao, a, b, erro,graph=False, maxIt=10000)
    funcao = defina e passe a funcao, sendo que ela deve ter apenas uma raiz em [a,b]
    a,b = intervalo
    erro = tal que |funcao((a+b)/2)| < erro finalizara o processo
    graph = Mostra o grafico do processo (default=False)
    maxIt = Numero maximo de iteracoes
    """
    # invertendo a e b
    if(a > b):
        buf = a
        a = b
        b = buf

    if(f(a)*f(b) > 0):
        raise ValueError("Funcao nao cruza eixo x no intervalo dado.")

    if(graph == True):
        x = np.linspace(a, b, (abs(a-b))/tol*100)
        plt.plot(x, f(x), color='black')
        path_array = np.array([[b, 0], [b, f(b)]])
        plt.plot(path_array[:, 0], path_array[:, 1], color="gray")
        path_array = np.array([[b, 0], [b, f(b)]])
        plt.plot(path_array[:, 0], path_array[:, 1], color="gray")

        plt.grid()
    counter = 0
    pmed = (a+b/2)
    while(True):
        # condicoes de parada
        if counter > maxIt:
            try:
                raise Exception(
                    "Limite de iteracoes excedido: 10000. Retornando mesmo asism.")
            finally:
                return pmed
        if(abs(f(pmed)) < tol):
            break

        pmed = (a+b)/2

        if(f(a)*f(pmed) < 0):
            if(graph == True):
                plt.plot([pmed], [f(pmed)], 'ro', marker='o',
                         markersize=3, color="red")
                plt.annotate(str(counter), (pmed, f(pmed)))
                path_array = np.array([[pmed, 0], [pmed, f(pmed)]])
                plt.plot(path_array[:, 0], path_array[:, 1], color="gray")
            b = pmed
            counter += 1
            continue
        else:
            if(graph == True):
                plt.plot([pmed], [f(pmed)], marker='o',
                         markersize=3, color="red")
                plt.annotate(str(counter), (pmed, f(pmed)))
                path_array = np.array([[pmed, 0], [pmed, f(pmed)]])
                plt.plot(path_array[:, 0], path_array[:, 1], color="gray")

            a = pmed
            counter += 1
            continue

    if(graph == True):
        plt.legend(['f(x)', 'y=x', "iteracoes"], loc='upper left')
        plt.show()

    return (a,pmed,b)


def pontoFixo(f, p0, tol, graph=False, maxIt=10000):
    """pontoFixo(funcao, a, b, erro, graph=False, maxIt=10000)
    funcao = defina e passe a funcao, sendo que ela deve cruzar y=x apenas uma vez em [a,b]
    a,b = intervalo
    erro = tal que |p(N) - p(N-1)| < erro finalizara o processo
    graph = Mostra o grafico do processo (default=False)
    maxIt = Numero maximo de iteracoes
    """
    counter = 0
    precision = int(-log10(tol))
    # add path lines
    try:
        p1 = f(p0)
        p1 = round(p1, precision)
    except OverflowError as e:
        print(e)
        print("Overflow Error. A", f.__code__.co_name, "(x) pode estar divergindo.Iteracao:",counter, "f(x)=", p0)
        return

    if(graph == True):
        x = np.linspace(p1-p0, p1+p0, (abs(p1+p0))/tol*100)
        plt.plot(x, f(x), color='black')
        plt.plot(x, x, color='gray')
        plt.grid()
        path_array = []

    while(True):
        # checando por numeros muito grandes/pequenos
        try:
            p1 = f(p0)
            p1 = round(p1, precision)
        except OverflowError as e:
            print(e)
            print("Overflow Error. A", f.__code__.co_name, "(x) pode estar divergindo.Iteracao:",
                  counter, "f(x)=", p0)
            return
        # condicoes de parada
        if counter > maxIt:
            try:
                raise Exception(
                    "Limite de iteracoes excedido: 10000. Retornando mesmo asism.")
            finally:
                if(graph == True):
                    plt.plot([p0], [p1], marker='o',
                             markersize=3, color="red", label="str(counter)")
                    plt.annotate(str(counter), (p0, p1))
                    path_array.append([p0, p0])
                    path_array.append([p0, p1])
                    path_array = np.array(path_array)
                    plt.plot(path_array[:, 0], path_array[:, 1])
                    plt.show()
                return p1
        if(abs(p1-p0) < tol):
            if(graph == True):
                plt.plot([p0], [p1], marker='o',
                         markersize=3, color="red", label="str(counter)")
                plt.annotate(str(counter), (p0, p1))
                path_array.append([p0, p0])
                path_array.append([p0, p1])
                path_array = np.array(path_array)
                plt.plot(path_array[:, 0], path_array[:, 1])
                plt.show()
            return p1

        if(graph == True):
            plt.plot([p0], [p1], marker='o',
                     markersize=3, color="red", label="str(counter)")
            plt.annotate(str(counter), (p0, p1))
            path_array.append([p0, p0])
            path_array.append([p0, p1])

        p0 = p1
        counter += 1
